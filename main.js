/**
 * Create a slideshow with fadeIn and fadeOut transitions
 * The slideshow should start automatically and it should use
 * fade effect when transitions to a new image
 */

 document.addEventListener('DOMContentLoaded', () => {
  // Getting slideshow element
  const mySlide = document.querySelector('#mySlide');

  // Calling plugin
  window.SlideShow(mySlide, 4);
});


( function (root) {
  root.SlideShow =  SlideShow;

    function SlideShow(element, timeSeconds = 4) {
    // Your Plugin code here
    let i = 0;
    let caption = document.getElementsByClassName("caption")[0];
    caption.innerText = element.children[0].getAttribute("data-caption");

    for (let index = 0; index < element.children.length-1; index++) {
      let images = element.children[index];
      //Set the opacity of the images in right order
      index == 0 ? images.style.opacity = 1 : images.style.opacity = 0;    
    }
    //Interval loop
    setInterval(() => {
      if (i == element.children.length  - 2 ) { //reset when it reaches the end of the array
        element.children[i].style.opacity = 0;
        element.children[0].style.opacity = 1;
        i = -1; 
      } else {
       element.children[i].style.opacity = 0;
       element.children[i+1].style.opacity = 1;
      }
      i++;
      caption.innerHTML = element.children[i].getAttribute("data-caption");
      console.log(caption);
    }, timeSeconds * 1000 );
  }
})(window);
